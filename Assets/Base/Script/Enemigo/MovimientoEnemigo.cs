using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class MovimientoEnemigo : MonoBehaviour
{
    public float fuerzaMovimiento;
    private AudioSource sonidoChoque;
    static bool enable;
    // Start is called before the first frame update
    void Start()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector2 dir = new Vector2(player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y) * fuerzaMovimiento;
        gameObject.GetComponent<Rigidbody2D>().AddForce(dir);
        sonidoChoque = GameObject.Find("Choque").GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            sonidoChoque.Play();
            GameObject.Find("PantallaDano").GetComponent<Animator>().SetBool("golpe", true);
            collision.gameObject.GetComponent<Reaparecer>().resetPosition();
            collision.gameObject.GetComponent<Reaparecer>().resetEnemigos();
        }
        else if(collision.gameObject.tag != "Plataformas" && collision.gameObject.tag != "Pincho")
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Respawn")
        {
            Destroy(gameObject);
        }
    }
}
