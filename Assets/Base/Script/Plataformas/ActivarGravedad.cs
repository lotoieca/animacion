using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ActivarGravedad : MonoBehaviour
{
    public bool aplicarEfecto = true;

    private float velocidadMovimiento = 1.5f;
    private bool caer = false;
    private Vector3 posicionInicial;
    // Start is called before the first frame update
    void Start()
    {
        posicionInicial = transform.position;
    }

    private void Update()
    {
        if(caer && aplicarEfecto)
        {
            Debug.Log(posicionInicial.y);
            transform.Translate(Vector3.down * velocidadMovimiento * Time.deltaTime);
            if(transform.position.y <= (posicionInicial.y - 4))
            {
                caer = false;
                transform.position = posicionInicial;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            caer = true;
        }
    }
    

}
