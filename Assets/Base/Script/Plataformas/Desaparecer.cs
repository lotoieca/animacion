using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Animator))]
public class Desaparecer : MonoBehaviour
{
    private Animator animCtrl;
    // Start is called before the first frame update
    void Start()
    {
        animCtrl = gameObject.GetComponent<Animator>();
    }

   private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            animCtrl.SetTrigger("enter");
        }
    }

}
