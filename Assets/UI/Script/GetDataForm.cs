using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDataForm : MonoBehaviour
{
    private Text textElement;
    private InputField inputElement;
    private Dropdown dropElement;
    private Toggle toggleElement;
    // Start is called before the first frame update
    void Start()
    {
        textElement = GameObject.Find("TextE").GetComponent<Text>();
        inputElement = GameObject.Find("InputFieldE").GetComponent<InputField>();
        dropElement = GameObject.Find("DropdownE").GetComponent<Dropdown>();
        toggleElement = GameObject.Find("ToggleE").GetComponent<Toggle>();
    }

    public void getChangeData()
    {
        textElement.text = "Texto Cambiado";
        inputElement.text = "Texto Modificado";
        dropElement.value = 2;
        toggleElement.isOn = false;
    }
}
