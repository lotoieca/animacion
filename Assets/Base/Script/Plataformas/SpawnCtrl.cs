using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCtrl : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject enemigo;
    public float tiempoParaDisparo;

    private bool disparo = false;
    private float tiempo;

    private void Start()
    {
        tiempo = Time.realtimeSinceStartup;
    }
    // Update is called once per frame
    void Update()
    {
        if (disparo && tiempo + tiempoParaDisparo < Time.realtimeSinceStartup)
        {
            Instantiate(enemigo, spawnPoint.position, Quaternion.identity);
            spawnPoint.gameObject.GetComponent<AudioSource>().Play();
            tiempo = Time.realtimeSinceStartup;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            disparo = true;
            if(Time.realtimeSinceStartup < tiempoParaDisparo)
            {
                tiempo -= tiempoParaDisparo;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            disparo = false;
        }
    }
}
