using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startMenu : MonoBehaviour
{
    private GameObject playerRef;
    private Animator animCtrl;
    // Start is called before the first frame update
    void Start()
    {
        playerRef = GameObject.FindGameObjectWithTag("Player");
        playerRef.SetActive(false);

        animCtrl = gameObject.GetComponent<Animator>();
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playGame()
    {
        animCtrl.SetBool("startGame",true);
    }

    public void Finish()
    {
        Debug.Log("Llamado a finish");
        playerRef.SetActive(true);
        playerRef.GetComponent<Reaparecer>().StarConfig();
        Destroy(gameObject);

    }
}
