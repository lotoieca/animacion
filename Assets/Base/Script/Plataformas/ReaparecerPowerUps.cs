using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReaparecerPowerUps : MonoBehaviour
{
    public GameObject powerPrefab;
    private List<Vector3> ubicacionesPowerUps = new List<Vector3>();

    private void Start()
    {
        GameObject[] ubicaciones = GameObject.FindGameObjectsWithTag("PowerUp");
        foreach(GameObject pw in ubicaciones)
        {
            ubicacionesPowerUps.Add(pw.transform.position);
        }
    }
    public void destroyPowerUps()
    {
        GameObject[] powerUpsAlive = GameObject.FindGameObjectsWithTag("PowerUp");
        foreach(GameObject power in powerUpsAlive)
        {
            Destroy(power);
        }

        foreach(Vector3 t in ubicacionesPowerUps)
        {
            Instantiate(powerPrefab, t, Quaternion.identity,gameObject.transform);
        }
    }


}
