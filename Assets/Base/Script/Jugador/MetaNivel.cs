using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetaNivel : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Finish")
        {
            collision.gameObject.GetComponent<AudioSource>().Play();
        }
    }
}
