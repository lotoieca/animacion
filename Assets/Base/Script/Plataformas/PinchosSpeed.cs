using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PolygonCollider2D))]
public class PinchosSpeed : MonoBehaviour
{
    public float velocidadAnimacion = 1;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Animator>().SetFloat("speed", velocidadAnimacion);        
    }

}
