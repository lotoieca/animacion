using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class MovimientoCtrl : MonoBehaviour
{
    public float velocidadMovimiento;
    public float fuerzaFrenado;
    private Rigidbody2D rig;

    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            float moveDir = Input.GetAxis("Horizontal") * velocidadMovimiento;
            rig.AddForce(new Vector2(moveDir, 0));
        }
        /*
        else
        {
            if (rig.velocity.x != 0)
            {
                //Debug.Log("No se esta moviendo en horizontal");
                if(rig.velocity.x > 0)
                {
                rig.velocity = new Vector2(rig.velocity.x-fuerzaFrenado,rig.velocity.y);
                }
                else
                {
                    rig.velocity = new Vector2(rig.velocity.x + fuerzaFrenado, rig.velocity.y);
                }
            }
        }*/
    }
}
