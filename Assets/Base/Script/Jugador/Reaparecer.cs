using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class Reaparecer : MonoBehaviour
{
    public SpriteRenderer mostrarPowerUp;
    private Rigidbody2D rig;
    private int vidas = 1;
    private GameObject[] vidasObjects;
    private ReaparecerPowerUps powerUpsControl;
    private AudioSource powerUpSonido;
    
    //UI CTRL
    private Text intentos;
    private int cuentaIntentos = 1;
    private Text timeCounter;
    private float tiempoInicioPartida;
    private float pauseTiempo;
    private GameObject pantallaVictoria;
    private GameObject pantallaPause;
    // Start is called before the first frame update
    void Awake()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
        powerUpsControl = GameObject.Find("powerUps").GetComponent<ReaparecerPowerUps>();
        powerUpSonido = GameObject.Find("/Audio/PowerUp").GetComponent<AudioSource>();
        vidasObjects = GameObject.FindGameObjectsWithTag("vidaContador");
        intentos = GameObject.Find("ContadorMuertes").GetComponent<Text>();
        timeCounter = GameObject.Find("tiempo").GetComponent<Text>();
        pantallaVictoria = GameObject.Find("WinScreen");
        pantallaPause = GameObject.Find("Pausa");

        StarConfig();
    }

    private void Update()
    {
        var ts = TimeSpan.FromSeconds(Time.realtimeSinceStartup - tiempoInicioPartida);
        timeCounter.text = string.Format("{0:00}:{1:00}", ts.Minutes, ts.Seconds);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Respawn")
        {
            Debug.Log("trigger activado");
            vidas = 1;
            resetPosition();
            resetEnemigos();
        }

        if(collision.gameObject.tag == "PowerUp")
        {
            Destroy(collision.gameObject);
            powerUpSonido.Play();
            if(vidas < 4)
            {
                vidas++;
                vidasObjects[vidas - 1].SetActive(true);
            }
        }

        if(collision.gameObject.tag == "Finish")
        {
            pantallaVictoria.SetActive(true);
            pantallaVictoria.GetComponent<WinCtrl>().SetScore(timeCounter.text, cuentaIntentos);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Pincho")
        {
            GameObject.Find("PantallaDano").GetComponent<Animator>().SetBool("golpe", true);
            resetEnemigos();
            resetPosition();
        }
    }
    //OBTIENE DA�O
    public void resetPosition()
    {
        vidas--;
        vidasObjects[vidas].GetComponent<Animator>().SetBool("damage",true);
        if (vidas == 0)
        {
            powerUpsControl.destroyPowerUps();
            transform.position = new Vector3(0, 0, 0);
            rig.velocity = Vector3.zero;
            rig.angularVelocity = 0f;
            foreach (GameObject corazon in vidasObjects)
            {
                corazon.SetActive(false);
            }
            tiempoInicioPartida = Time.realtimeSinceStartup;
            vidas = 1;
            vidasObjects[vidas - 1].SetActive(true);
            cuentaIntentos++;
            intentos.text = "Intentos " + cuentaIntentos;
        }
    }

    public void resetEnemigos()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("Enemigo");
        foreach(GameObject e in enemigos)
        {
            Destroy(e);
        }
    }

    //Limpian y restablen los datos iniciales
    public void StarConfig()
    {
        vidas = 1;
        cuentaIntentos = 1;
        Time.timeScale = 1;
        transform.position = Vector3.zero;
        pantallaVictoria.SetActive(false);
        pantallaPause.SetActive(false);
        tiempoInicioPartida = Time.realtimeSinceStartup;
        intentos.text = "Intentos " + cuentaIntentos;
        foreach (GameObject corazon in vidasObjects)
        {
            corazon.SetActive(false);
        }
        vidasObjects[vidas - 1].SetActive(true);
    }

    public void SetPausa()
    {
        pauseTiempo = Time.realtimeSinceStartup;
        pantallaPause.SetActive(true);
        pantallaPause.GetComponent<PausaCtrl>().IniciaPause();
    }

    public void Despausar()
    {
        tiempoInicioPartida += Time.realtimeSinceStartup - pauseTiempo;
        pantallaPause.SetActive(false);
    }
}
