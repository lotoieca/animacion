using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private Vector2 efectoParallaxMultiplicador;

    private Transform camaraTransform;
    private Vector3 ultimaPosicionCamara;
    // Start is called before the first frame update
    void Start()
    {
        camaraTransform = Camera.main.transform;
        ultimaPosicionCamara = camaraTransform.position;
    }

    // Update is called once per frame Before UPDATE
    void LateUpdate()
    {
        Vector3 deltaMovimiento = camaraTransform.position - ultimaPosicionCamara;
        transform.position += new Vector3(deltaMovimiento.x * efectoParallaxMultiplicador.x, deltaMovimiento.y * efectoParallaxMultiplicador.y, 0f);
        ultimaPosicionCamara = camaraTransform.position;
    }
}
