using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class animacionSimple : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump")){
            gameObject.GetComponent<Animator>().SetTrigger("salto");
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            gameObject.GetComponent<Animator>().SetBool("movimiento", true);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            gameObject.GetComponent<Animator>().SetBool("movimiento", false);
        }

    }

    public void eventoSalto()
    {
        Debug.Log("La animacion de salto esta en su punto mas alto");
    }
}
