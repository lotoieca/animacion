using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody2D))]
public class Salto : MonoBehaviour
{
    public float fuerzaSalto;
    private Rigidbody2D rig;
    private bool estaEnTierra = false;
    private AudioSource audioSalto;
    private int saltosRestantes = 2;

    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
        audioSalto = gameObject.GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && saltosRestantes > 0)
        {
            rig.AddForce(new Vector2(0, fuerzaSalto));
            audioSalto.Play();
            estaEnTierra = false;
            saltosRestantes--;
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Plataformas" && !estaEnTierra)
        {
            estaEnTierra = true;
            saltosRestantes = 2;
        }
    }
}
