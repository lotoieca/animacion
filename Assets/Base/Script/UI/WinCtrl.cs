using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinCtrl : MonoBehaviour
{
    public Text Score;
    private GameObject player;

    public void SetScore(string tiempo, int vidas)
    {
        Score.text = "Tiempo:" + tiempo + " | Intentos:" + vidas.ToString();
        PlayerPrefs.SetString("record", Score.text);
        player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);
    }

    public void VolverIntentar()
    {
        SceneManager.LoadScene("UI");
        //player.transform.position = Vector3.zero;
        //player.SetActive(true);
        //player.GetComponent<Reaparecer>().StarConfig();
    }
}
