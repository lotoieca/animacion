using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausaCtrl : MonoBehaviour
{
    private GameObject player;

    public void IniciaPause()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);
        Time.timeScale = 0;
    }

    public void Despausar()
    {
        player.SetActive(true);
        player.GetComponent<Reaparecer>().Despausar();
        Time.timeScale = 1;
    }
}
